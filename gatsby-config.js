require(`dotenv`).config({
  path: `.env`,
});

const shouldAnalyseBundle = process.env.ANALYSE_BUNDLE;

module.exports = {
  siteMetadata: {
    siteTitle: `🚀getpawarful`,
    siteHeadline: "🚀getpawrful - Code ➡️ Deploy ➡️ Automate 🔃",
    author: "@getpawarful",
    siteUrl: "https://adityapawar.com",
    siteDescription: "🚀getpawrful - Code ➡️ Deploy ➡️ Automate 🔃",
    siteLanguage: "en",
    siteImage: `/banner.jpg`,
    siteTitleAlt: `🚀getpawrful - Code ➡️ Deploy ➡️ Automate 🔃`,
  },
  plugins: [
    {
      resolve: `@lekoarts/gatsby-theme-minimal-blog`,
      // See the theme's README for all available options
      options: {
        feedTitle: "🚀getpawrful - Code ➡️ Deploy ➡️ Automate 🔃",
        navigation: [
          {
            title: `Blog`,
            slug: `/blog`,
          },
          {
            title: `About`,
            slug: `/about`,
          },
        ],
        externalLinks: [
          {
            name: `Twitter`,
            url: `https://twitter.com/getpawarful`,
          },
          {
            name: `Instagram`,
            url: `https://www.instagram.com/getpawarful/`,
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GOOGLE_ANALYTICS_ID,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `🚀getpawrful - Code ➡️ Deploy ➡️ Automate 🔃`,
        short_name: `getpawarful`,
        description: `A comprehensive guide for converting ideas into products and everything in between. I write about software development, deployments, and managing scalable software.`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#6B46C1`,
        display: `standalone`,
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-gatsby-cloud`,
    `gatsby-plugin-netlify`,
    shouldAnalyseBundle && {
      resolve: `gatsby-plugin-webpack-bundle-analyser-v2`,
      options: {
        analyzerMode: `static`,
        reportFilename: `_bundle.html`,
        openAnalyzer: false,
      },
    },
  ].filter(Boolean),
};
