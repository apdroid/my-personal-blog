---
title: "How to write clean conditions in Js"
description: "A better way of writing simple and reable condition statements in js"
date: 2021-04-02
slug: "/js-clean-conditions"
tags:
    - Js
# canonicalUrl: "https://random-blog-about-curses.com"
# banner: "/banner.jpg"
---

Writing simple and readable code one of the qualities that differentiates good developer from the rest. If you have done React then I am sure you must have encountered these two conditional code blocks:

#### Validation Function
This is typically used in form validation, disabling component etc 
```js
const validationFunc = (value) => {
  if (
    value === "a" ||
    value === "b" ||
    value === "c" ||
    value === "d" ||
    value === "e" ||
    value === "f"
  )
    return true;
  return false;
};
```
A better way to write the same statement: 

```js
const validationFunc = (value) => {
  const trueValues = ["a", "b", "c", "d", "e", "f"];
  return trueValues.includes(value);
};
```

#### OnKeyPress function
This is very commonly used for executing different functions according to input value:
```js
const onKeyPress = (value) => {
  if (value === "w") {
    moveUp();
  } else if (value === "s") {
    moveDown();
  } else if (value === "a") {
    moveLeft();
  } else if (value === "d") {
    moveRight();
  }
};
```

A better way could be using switch statement but here is an even more cleaner way to do it:
```js
const onKeyPress = (value) => {
  const functionMap = {
    w: moveUp,
    s: moveDown,
    a: moveLeft,
    d: moveRight,
  };
  functionMap[value] && functionMap[value]();
};
```
