---
title: "Docker saved countless days dealing with IT guys…"
description: "Docker is an container runtime engine which can be used to run application without acutally installing them"
date: 2021-04-18
slug: "/docker-saved-countless-days"
tags:
    - Docker
    - Hacks
# canonicalUrl: "https://random-blog-about-curses.com"
# banner: "/banner.jpg"
---

Being a good developer is hard, often it means finding out efficient ways to solve a problem. Out of all those things that can be solved with code, I am talking about the one where you decide to use someone else code. Sometimes it is not just code-snippets, it is a collection of snippets bundled together as an application or a program. You want to try a new application. Let us say you find advantages of non-relational databases over relational and you want to try out how they really perform for your use case. Conventionally to do so you'll first have to install a non-relational database in your machine, which would need an installation request to the IT team. Meanwhile, the IT team looks into your request, your curiosity is in its peak. You'll wait for a day, maybe two, and try the IT team to rush up the installation process. By the time a non-relational database is installed on your machine, you might again be burdened with another task important task waiting in the queue and more waiting. Pretty common. In short, I am talking about  something instantaneous, something you can spin-off for a test and kill it back to normal. To get around this mundane process with IT guys, you can leverage the capability offered by Docker. Docker is a container runtime engine that lets you run isolated containers(binaries) on your host machine. How will this solve your problem? If you have docker running on your machine and you want to try out MongoDB, all you need to do is pull MongoDB's image and you're good to go. You'll have a running instance on MongoDB which can be exposed locally through localhost. There you saved countess hours with IT guys by using docker. Cheers :)
